package nl.elstarit.snoug.servlet.webapp.jee;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import nl.elstarit.snoug.servlet.shared.worker.ServletWorker;

/**
 * Servlet implementation class MyDemoServlet
 */
@WebServlet("/MyDemoServlet")
public class MyDemoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ServletWorker worker =  ServletWorker.INSTANCE;
	private ObjectMapper mapper = new ObjectMapper();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MyDemoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println("Get method my Demo Servlet");
		response.getWriter().println("Get method my Demo Servlet!!!!!");
		
		//get demo text from shared plugin
		response.getWriter().println(worker.demoText());
		
		//get data from Cloudant
		//Object json = mapper.readValue(worker.loadCompaniesFromCloudant(), Object.class);
		//response.getWriter().println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Post method my Demo Servlet");
	}
	
	public void destroy() {
	      System.out.println("Destroy JEE Demo Servlet");
	}
	
	public void init() throws ServletException {
	      // Do required initialization
	      System.out.println("Intitialize JEE Demo Servlet second try");
	}

}