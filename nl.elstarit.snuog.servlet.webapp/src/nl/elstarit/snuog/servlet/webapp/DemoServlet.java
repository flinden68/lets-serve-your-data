package nl.elstarit.snuog.servlet.webapp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.ibm.domino.osgi.core.context.ContextInfo;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.Session;
import nl.elstarit.snoug.servlet.shared.model.Post;
import nl.elstarit.snoug.servlet.shared.util.Utilities;
import nl.elstarit.snoug.servlet.shared.worker.ServletWorker;

public class DemoServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private ServletWorker worker =  ServletWorker.INSTANCE;
	private ObjectMapper mapper = new ObjectMapper();
	
	
	public DemoServlet(){
		
	}
	
	public void init() throws ServletException {
	      // Do required initialization
	      System.out.println("Intitialize Domino OSGi Demo Servlet");
	      try{
	    	  worker.init(true);
	      }catch(Exception e){
				e.printStackTrace();
			}
	      
	}
	
	public void destroy() {
	      System.out.println("Destroy Domino OSGiDemo Servlet");
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String status = "";
		Database db = null;

		int statuscode = HttpServletResponse.SC_OK;
		try{
			Session session = ContextInfo.getUserSession();
			Utilities.logDebugInformation("Get method my Demo Servlet");
				db = ContextInfo.getUserDatabase();
				if (db != null) {
					if(worker.isUserAuthenticated(session,db) && worker.canUserRead(session, db)){
						status = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(worker.posts(session, db));
					}else{
						status = ContextInfo.getUserSession().getEffectiveUserName() +" is not allowed to read data";
						statuscode = HttpServletResponse.SC_FORBIDDEN;
					}
				}else{
					status = "servlet does not run on a database";
					statuscode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
				}
			
		}catch(Exception e){
			Utilities.getLogger().log(Level.WARNING, e.getMessage());
		}
		response.setStatus(statuscode);
		response.getWriter().println("Status: " + status);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilities.logDebugInformation("Post method my Demo Servlet");
		String status = "";
		Database db = null;
		int statuscode = HttpServletResponse.SC_OK;

		try {
				Session session=ContextInfo.getUserSession();		
				db = ContextInfo.getUserDatabase();
				if (db != null) {
					if(worker.isUserAuthenticated(session,db) && worker.canUserPost(session, db)){
						if (request.getContentLength() > 0) {
				            char[] cbuf = new char[request.getContentLength()];
				            request.getReader().read(cbuf);
				            String data = new String(cbuf);
				            Utilities.logDebugInformation("data="+data);
				            Post post = mapper.readValue(data, Post.class);
				            Utilities.logDebugInformation("post="+post);
				            status = Boolean.toString(worker.post(session, db, post));
				        } else {
				            status = "No content has been posted";
				            statuscode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
				        }
	
					}else{
						status = ContextInfo.getUserSession().getEffectiveUserName() +" is not allowed to write data";
						statuscode = HttpServletResponse.SC_FORBIDDEN;
					}
				}else{
					status = "servlet does not run on a database";
					statuscode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
				}
			
			
		}catch (Throwable e) {
			status = e.getMessage();
			statuscode = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
			Utilities.getLogger().log(Level.WARNING, e.getMessage());
		}finally{
			try {
				db.recycle();
			} catch (NotesException e) {
				// TODO Auto-generated catch block
				Utilities.getLogger().log(Level.WARNING, e.getMessage());
			}
		}
		response.setStatus(statuscode);
		response.getWriter().println("Status: " + status);
	}

}