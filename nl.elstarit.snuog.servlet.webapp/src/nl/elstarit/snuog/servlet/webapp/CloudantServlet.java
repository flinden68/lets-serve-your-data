package nl.elstarit.snuog.servlet.webapp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.ibm.domino.osgi.core.context.ContextInfo;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.Session;
import nl.elstarit.snoug.servlet.shared.model.Post;
import nl.elstarit.snoug.servlet.shared.util.Utilities;
import nl.elstarit.snoug.servlet.shared.worker.ServletWorker;

public class CloudantServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private ServletWorker worker =  ServletWorker.INSTANCE;
	private ObjectMapper mapper = new ObjectMapper();
	
	
	public CloudantServlet(){
		
	}
	
	public void init() throws ServletException {
	      // Do required initialization
	      System.out.println("Intitialize Domino OSGi Demo Servlet");
	      try{
	    	  worker.init(true);
	      }catch(Exception e){
				e.printStackTrace();
			}
	      
	}
	
	public void destroy() {
	      System.out.println("Destroy Domino OSGiDemo Servlet");
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String status = "";

		int statuscode = HttpServletResponse.SC_OK;
		try{
			Utilities.logDebugInformation("Get method Cloudant Servlet");

			//get data from cloudant
			Object json = mapper.readValue(worker.loadCompaniesFromCloudant(), Object.class);
			response.getWriter().println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json));
			
		}catch(Exception e){
			Utilities.getLogger().log(Level.WARNING, e.getMessage());
		}
		response.setStatus(statuscode);
		response.getWriter().println("Status: " + status);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilities.logDebugInformation("Post method Cloudant Servlet");
		String status = "Post method Cloudant Servlet not implemented";
		int statuscode = HttpServletResponse.SC_OK;
		
		response.setStatus(statuscode);
		response.getWriter().println("Status: " + status);
	}

}