package nl.elstarit.snoug.servlet.dao;

import java.util.Date;

import nl.elstarit.snoug.servlet.service.CloudantService;

public class BaseDAO {
	protected CloudantService cloudantService = new CloudantService();
    protected static final String SEARCH_QUERYREPLACE = "{QUERY}";
    
    public void connectToService() {
        if (!cloudantService.isConnected()) {
            cloudantService.connect();
        }
    }
    
    public CloudantService getCloudantService() {
        return cloudantService;
    }
    
    public void setCloudantService(CloudantService cloudantService) {
        this.cloudantService = cloudantService;
    }
    
}
