package nl.elstarit.snoug.servlet.dao;

public class JobDAO extends BaseDAO {

    private static final String DESIGN_DOC = "job";
    private static final String VIEW_NAME = "jobs";
    

    @SuppressWarnings("unchecked")
    public String read() {
        System.out.println("READ JOBS VIA DAO LAYER");
        return cloudantService.findAllDocumentFromView(
                DESIGN_DOC, VIEW_NAME);
    }

}
