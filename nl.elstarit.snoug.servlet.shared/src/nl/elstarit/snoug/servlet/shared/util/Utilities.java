package nl.elstarit.snoug.servlet.shared.util;

import java.text.NumberFormat;
import java.util.logging.Logger;

import lotus.domino.Base;

public class Utilities {
	
	private static Logger logger = Logger.getLogger("");
	
	public static String getMemoryStatus() {
		Runtime rt = Runtime.getRuntime();
		NumberFormat nf = NumberFormat.getInstance();
		nf.setGroupingUsed(true);
		nf.setMinimumFractionDigits(0);
		long total_mem = rt.totalMemory();
		long free_mem = rt.freeMemory();
		long used_mem = total_mem - free_mem;
		return "Amount of used memory/free memory: " + nf.format(used_mem/1000) + "KB / " + nf.format(free_mem/1000)+" KB";
	}
	
	
	public static void recycleObjects(final Object... dominoObjects) {
		try{
		    for (Object dominoObject : dominoObjects) {
		        if (null != dominoObject) {
		            if (dominoObject instanceof Base) {
		              ((Base)dominoObject).recycle();
		            }
		        }
		    }
		} catch (Exception e) {
			logger.warning(e.getMessage());
		}
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		Utilities.logger = logger;
	}
	
	public static void logDebugInformation(String msg){
		logger.info(msg);
	}

}

