package nl.elstarit.snoug.servlet.shared.model;

public class Post {
	
	private String name;
	private String emailaddress;
	
	public Post(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailaddress() {
		return emailaddress;
	}

	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}

	@Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("{");
        sb.append("\"name\":\"" + name + "\",");
        sb.append("\"emailaddress\":\"" + emailaddress + "\"");
        sb.append("}");
        return sb.toString();

    }
}
