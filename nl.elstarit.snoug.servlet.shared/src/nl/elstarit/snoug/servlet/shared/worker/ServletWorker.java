package nl.elstarit.snoug.servlet.shared.worker;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.fasterxml.jackson.databind.ObjectMapper;

import nl.elstarit.snoug.servlet.dao.JobDAO;
import nl.elstarit.snoug.servlet.shared.model.Post;
import nl.elstarit.snoug.servlet.shared.util.Utilities;
import lotus.domino.ACL;
import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.View;

public enum ServletWorker {
	INSTANCE;
	private static Logger logger = Logger.getLogger(ServletWorker.class.getName());
	private JobDAO jobDAO = new JobDAO();
	private ObjectMapper mapper = new ObjectMapper();
	
	public String demoText(){
		return "Demo text from Worker";
	}
	
	public void init(boolean toFile) throws SecurityException, IOException{
		if(toFile){
			SimpleDateFormat formatter = new  SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date timestamp = new Date();
			String formatTimeStamp = formatter.format(timestamp);
			String fileName = formatTimeStamp.replaceAll("[:-]", "_");
			
			FileHandler fh = new FileHandler(System.getProperty("java.io.tmpdir")+System.getProperty("file.separator")+fileName+".txt");

	        fh.setFormatter(new SimpleFormatter());
			Utilities.getLogger().setLevel(Level.INFO);
			Utilities.getLogger().addHandler(fh);
		}
	}
	
	public boolean isUserAuthenticated(Session session, Database db) throws Exception{
		int accessLevel = db.queryAccess(session.getUserName());

		if(!"anonymous".equals(session.getUserName())){
			return true;
		}
		return false;		
	}
	
	public boolean canUserPost(Session session, Database db) throws Exception{
		int accessLevel = db.queryAccess(session.getUserName());
		if(accessLevel>=ACL.LEVEL_EDITOR){
			return true;
		}
		return false;
	}
	
	public boolean canUserRead(Session session, Database db) throws Exception{
		int accessLevel = db.queryAccess(session.getUserName());
		if(accessLevel>=ACL.LEVEL_READER){
			return true;
		}
		return false;
	}
	
	public boolean post(Session session, Database db, Post post){
		boolean success = false;
		Document doc = null;
		try{
			doc = db.createDocument();
			doc.replaceItemValue("Form", "post");
			doc.replaceItemValue("name", post.getName());
			doc.replaceItemValue("emailaddress", post.getEmailaddress());
			
			doc.save();
			Utilities.logDebugInformation("Document is has been created");
			success = true;
		}catch(Exception e){
			logger.log(Level.WARNING, e.getMessage());
		}finally{
			try {
				doc.recycle();
			} catch (NotesException e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
		}
		
		return success;
	}
	
	public List<Post> posts(Session session, Database db){
		List<Post> posts = new ArrayList<Post>();
		Document doc = null;
		Document docNext = null;
		View vw = null;

		try{
			vw = db.getView("posts");
			doc = vw.getFirstDocument();
			
			while(doc != null){
				docNext = vw.getNextDocument(doc);
				Post post = new Post();
				post.setName(doc.getItemValueString("name"));
				post.setEmailaddress(doc.getItemValueString("emailaddress"));
				
				posts.add(post);
				doc.recycle();
				doc = docNext;
			}
			
		}catch(Exception e){
			logger.log(Level.WARNING, e.getMessage());
		}finally{
			try {
				if(doc != null){
					doc.recycle();
				}
				vw.recycle();
				if(docNext != null){
					docNext.recycle();
				}
			} catch (NotesException e) {
				logger.log(Level.SEVERE, e.getMessage());
			}
		}
		
		return posts;
	}
	
	public String loadCompaniesFromCloudant(){
		return jobDAO.read();
	}

}