package nl.elstarit.snoug.servlet.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;

public class RestUtil {
Executor executor = Executor.newInstance();
	
	/**
	 * Send basic GET request with authorization header
	 */
	public Response get(String url, String auth) throws URISyntaxException, IOException {
		URI normUri = new URI(url).normalize();
		Request getRequest = Request.Get(normUri);
		//if(StringUtil.isNotEmpty(auth)) {
			getRequest.addHeader("Authorization", auth);
		//}
		Response response = executor.execute(getRequest);
		return response;
	}
	
	/**
	 * Send basic GET request
	 */
	public Response get(String url) throws URISyntaxException, IOException {
		return get(url, null);
	}

	
	/**
	 * Convert a JSON String into a JsonJavaObject
	 * @param data - The JSON data in String format
	 * @return JsonJavaObject containing the JSON data
	 */
	/*public JsonJavaObject parse(String data) {
		JsonJavaObject jsonData = null;
		try {
			jsonData = (JsonJavaObject) JsonParser.fromJson(JsonJavaFactory.instanceEx, data);
		} catch (JsonException e) {
			Platform.getInstance().log(e);
		}
		return jsonData;
	}*/
}
