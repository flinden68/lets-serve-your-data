package nl.elstarit.snoug.servlet.service;

import javax.xml.bind.DatatypeConverter;

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Response;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CloudantService {
	private static final String SERVICE_NAME = "cloudantNoSQLDB";
    private String account = "f44a2403-965e-4345-bc84-4fd485047735-bluemix:e0ab6592f8d6a0192d8d819fbe656a112f54fc82464a46ca9005cdc63b396d4a@f44a2403-965e-4345-bc84-4fd485047735-bluemix";
    private String password = "e0ab6592f8d6a0192d8d819fbe656a112f54fc82464a46ca9005cdc63b396d4a";
    private String username = "f44a2403-965e-4345-bc84-4fd485047735-bluemix";
    private String cloudantDb = "hrassistant";
    private String baseUrl = "https://f44a2403-965e-4345-bc84-4fd485047735-bluemix:e0ab6592f8d6a0192d8d819fbe656a112f54fc82464a46ca9005cdc63b396d4a@f44a2403-965e-4345-bc84-4fd485047735-bluemix.cloudant.com";
    
    private boolean connected = false;

    private RestUtil rest = new RestUtil();

    public CloudantService() {
        
    }
    
    public void connect() {
        connected = true;
    }

    
    public boolean isConnected() {
        return connected;
    }
    
    public String getAuthorizationHeader(){
		String auth   = getUsername() + ":" + getPassword();
		String header = "Basic " + DatatypeConverter.printBase64Binary(auth.getBytes());
		return header;
	}
    /**
     * 
     * @param cls
     * @param designDoc
     * @param viewName
     * @param keyType
     * @param limit
     * @return
     */
    public String findAllDocumentFromView(
            final String designDoc, final String viewName) {
        try {
        	String getUrl = getBaseUrl() + "/" + cloudantDb + "/_design/" + designDoc + "/_view/" + viewName + "?limit=100&reduce=false&include_docs=true&conflicts=true";

        	Response response = rest.get(getUrl, getAuthorizationHeader());
        	
        	HttpResponse httpResponse = response.returnResponse();
    		int statusCode = httpResponse.getStatusLine().getStatusCode();
    		if(statusCode == 404) {
    			return null;
    		}else{
    			String content = EntityUtils.toString(httpResponse.getEntity());
    			return content;
    		}
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return null;
    }
    
    
 
    /**
     * Getters and Setters
     */
    
    public String getAccount() {
        return account;
    }
    
    public void setAccount(String account) {
        this.account = account;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getCloudantDb() {
        return cloudantDb;
    }
    
    public void setCloudantDb(String cloudantDb) {
        this.cloudantDb = cloudantDb;
    }
    
    public void setConnected(boolean connected) {
        this.connected = connected;
    }

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
    
}
